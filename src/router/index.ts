import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import MainMenu from "../views/MainMenu.vue";
import PointofSellView from "../views/PointofSellView.vue";
import StorageView from "@/views/StorageView.vue";
import CustomerView from "../views/customer/CustomerView.vue";
import ProductView from "../views/product/ProductView.vue";
import StoreView from "../views/store/StoreView.vue";
import UserViewVue from "@/views/user/UserView.vue";
import EmployeeViewVue from "@/views/employee/EmployeeView.vue";
import LoginView from "@/views/LoginView.vue";
import MaterialViewVue from "@/views/material/MaterialView.vue";
import ProductPaginationView from "@/views/ProductPaginationView.vue";
import HistorySellVue from "@/views/sell/HistorySell.vue";
import checkinViewVue from "@/views/checkin_out/checkinView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: MainMenu,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/about",
      name: "about",
      components: {
        default: import("../views/AboutView.vue"),
        menu: () => import("@/components/menus/AboutMenu.vue"),
        header: () => import("@/components/headers/AboutHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/main-menu",
      name: "Main Menu",
      components: {
        default: MainMenu,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/customer",
      name: "Customer",
      components: {
        default: CustomerView,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/point-of-sell",
      name: "Point of Sell",
      components: {
        default: PointofSellView,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/storage",
      name: "Storage",
      components: {
        default: StorageView,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/product",
      name: "Product",
      components: {
        default: ProductView,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/employee",
      name: "Employee",
      components: {
        default: EmployeeViewVue,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/user",
      name: "User",
      components: {
        default: UserViewVue,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/checkinouts",
      name: "checkinouts",
      components: {
        default: checkinViewVue,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/TinloginView",
      name: "Tinlogin",
      component: () => import("../views/TinSubscribe.vue"),
    },
    {
      path: "/store",
      name: "Store",
      components: {
        default: StoreView,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/material",
      name: "Material",
      components: {
        default: MaterialViewVue,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/login",
      name: "login",
      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/product-page",
      name: "product-page",
      components: {
        default: ProductPaginationView,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/history",
      name: "history",
      components: {
        default: HistorySellVue,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
  ],
});
function isLogin() {
  const user = localStorage.getItem("user");
  if (user) {
    return true;
  }
  return false;
}
router.beforeEach((to, from) => {
  if (to.meta.requiresAuth && !isLogin()) {
    return {
      path: "/login",
      query: { redirect: to.fullPath },
    };
  }
});

export default router;
