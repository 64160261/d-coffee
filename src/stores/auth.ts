import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import router from "@/router";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useAuthStore = defineStore("auth", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authName = ref("");

  const getUser = () => {
    const userString = localStorage.getItem("user");
    if (!userString) return null;
    const user = JSON.parse(userString ?? "");
    return user;
  };

  const getCustomer = () => {
    const customerString = localStorage.getItem("customer");
    if (!customerString) return null;
    const customer = JSON.parse(customerString ?? "");
    return customer;
  };

  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  };
  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(username, password);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      router.push("/");
    } catch (e) {
      messageStore.showError("username หรือ password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
    // localStorage.setItem("token", userName);
  };
  const logout = () => {
    //authName.value = "";
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.replace("/login");
  };

  return { login, logout, isLogin, getUser, getCustomer };
});
