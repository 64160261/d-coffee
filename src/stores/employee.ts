import { ref, watch } from "vue";
import { defineStore } from "pinia";
import employeeService from "@/services/employee";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type { Employee } from "@/types/Employee";

export const useEmployeeStore = defineStore("Employee", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const employees = ref<Employee[]>([]);
  const editedEmployee = ref<Employee>({
    name: "",
    tel: "",
    email: "",
    position: "",
    hourly_wage: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedEmployee.value = {
        name: "",
        tel: "",
        email: "",
        position: "",
        hourly_wage: 0,
      };
    }
  });
  async function getEmployees() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployees();
      employees.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveEmployee() {
    loadingStore.isLoading = true;
    try {
      if (editedEmployee.value.id) {
        const res = await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        const res = await employeeService.saveEmployee(editedEmployee.value);
      }

      dialog.value = false;
      await getEmployees();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Employee ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteEmployee(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.deleteEmployee(id);
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Employee ได้");
    }
    loadingStore.isLoading = false;
  }
  function editEmployee(user: Employee) {
    editedEmployee.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }
  return {
    employees,
    getEmployees,
    dialog,
    editedEmployee,
    saveEmployee,
    editEmployee,
    deleteEmployee,
  };
});
