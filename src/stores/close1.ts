import { ref, watch } from "vue";
import { defineStore } from "pinia";

export const useClose1Store = defineStore("close1", () => {
  const dialog = ref(false);

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
  });
  return {
    dialog,
  };
});
