import { ref, watch } from "vue";
import { defineStore } from "pinia";

export const useCloseStore = defineStore("close", () => {
  const dialog = ref(false);

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
  });
  return {
    dialog,
  };
});
