import { ref, watch } from "vue";
import { defineStore } from "pinia";
import customerService from "@/services/customer";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type { Customer } from "@/types/Customer";

export const useCustomerStore = defineStore("Customer", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const users = ref<Customer[]>([]);
  const editedCustomer = ref<Customer>({
    name: "",
    tel: "",
    point: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCustomer.value = {
        name: "",
        tel: "",
        point: 0,
      };
    }
  });
  async function getCustomers() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.getCustomersByQuery();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getCustomersByPoint() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.getCustomerByPoint();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveCustomer() {
    loadingStore.isLoading = true;
    try {
      if (editedCustomer.value.id) {
        const res = await customerService.updateCustomer(
          editedCustomer.value.id,
          editedCustomer.value
        );
      } else {
        const res = await customerService.saveCustomer(editedCustomer.value);
      }

      dialog.value = false;
      await getCustomers();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteCustomer(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.deleteCustomer(id);
      await getCustomers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Customer ได้");
    }
    loadingStore.isLoading = false;
  }
  function editCustomer(user: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(user));

    dialog.value = true;
  }
  return {
    users,
    getCustomers,
    dialog,
    editedCustomer,
    saveCustomer,
    editCustomer,
    deleteCustomer,
    getCustomersByPoint,
  };
});
