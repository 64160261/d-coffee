import { ref, watch } from "vue";
import { defineStore } from "pinia";
import storeService from "@/services/store";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type { Store } from "@/types/Store";

export const useStoreStore = defineStore("Store", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const stores = ref<Store[]>([]);
  const catagory = ref(1);
  const editedStore = ref<Store>({
    name: "",
    province: "",
    district: "",
    subdistrict: "",
    zip: "",
    tel: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedStore.value = {
        name: "",
        province: "",
        district: "",
        subdistrict: "",
        zip: "",
        tel: "",
      };
    }
  });
  async function getStores() {
    loadingStore.isLoading = true;
    try {
      const res = await storeService.getStores();
      stores.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Store ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveStore() {
    loadingStore.isLoading = true;
    try {
      if (editedStore.value.id!) {
        const res = await storeService.updateStores(
          editedStore.value.id,
          editedStore.value
        );
      } else {
        const res = await storeService.saveStores(editedStore.value);
      }

      dialog.value = false;
      await getStores();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Store ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteStore(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await storeService.deleteStores(id);
      await getStores();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Store ได้");
    }
    loadingStore.isLoading = false;
  }
  function editStore(store: Store) {
    editedStore.value = JSON.parse(JSON.stringify(store));
    dialog.value = true;
  }
  return {
    stores,
    getStores,
    dialog,
    editedStore,
    saveStore,
    editStore,
    deleteStore,
    catagory,
  };
});
