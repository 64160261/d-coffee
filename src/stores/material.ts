import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Material from "@/types/Material";
import materialService from "@/services/material";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useMaterialStore = defineStore("Material", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const alert = ref(false);
  const materials = ref<Material[]>([]);
  const catagory = ref();

  const editedMaterial = ref<Material & { files: File[] }>({
    name: "",
    minQuantity: 0,
    quantity: 0,
    unit: 0,
    price: 0,
    image: "no_img_avaliable.jpg",
    files: [],
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedMaterial.value = {
        name: "",
        minQuantity: 0,
        quantity: 0,
        unit: 0,
        price: 0,
        image: "no_img_avaliable.jpg",
        files: [],
      };
    }
  });

  const showLowStockAlert = computed(() => {
    const lowStockMaterials = materials.value.filter(
      (material) => material.quantity < material.minQuantity
    );
    if (lowStockMaterials.length > 0) {
      alert.value = true;
    }
    return lowStockMaterials;
  });

  async function getMaterials() {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.getMaterials();
      materials.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Material ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getlowMaterial() {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.getlowMaterial();
      materials.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Material ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveMaterial() {
    console.log(editedMaterial.value.id);
    loadingStore.isLoading = true;
    try {
      if (editedMaterial.value.id) {
        const res = await materialService.updateMaterial(
          editedMaterial.value.id,
          editedMaterial.value
        );
      } else {
        const res = await materialService.saveMaterial(editedMaterial.value);
      }

      dialog.value = false;
      await getMaterials();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Material ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteMaterial(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.deleteMaterial(id);
      await getMaterials();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Material ได้");
    }
    loadingStore.isLoading = false;
  }
  function editMaterial(material: Material) {
    editedMaterial.value = JSON.parse(JSON.stringify(material));
    dialog.value = true;
    ///
  }

  return {
    showLowStockAlert,
    materials,
    getMaterials,
    dialog,
    editedMaterial,
    saveMaterial,
    editMaterial,
    deleteMaterial,
    catagory,
    getlowMaterial,
  };
});
