import { ref, watch } from "vue";
import { defineStore } from "pinia";
import checkService from "@/services/checkin";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Check from "@/types/Check";

export const useCheckStore = defineStore("Check", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const checks = ref<Check[]>([]);
  const date = ref("");
  const today = new Date();
  date.value =
    today.getDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear();
  const editedCheck = ref<Check>({
    date: date.value,
    time_in: "",
    time_out: "",
    total_hour: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCheck.value = {
        date: date.value,
        time_in: "",
        time_out: "",
        total_hour: "",
      };
    }
  });
  async function getChecks() {
    loadingStore.isLoading = true;
    try {
      const res = await checkService.getChecks();
      checks.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Time ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveCheck() {
    loadingStore.isLoading = true;
    try {
      if (editedCheck.value.id) {
        const res = await checkService.updateCheck(
          editedCheck.value.id,
          editedCheck.value
        );
      } else {
        const res = await checkService.saveCheck(editedCheck.value);
      }

      dialog.value = false;
      await getChecks();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Time ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editCheck(check: Check) {
    editedCheck.value = JSON.parse(JSON.stringify(check));

    dialog.value = true;
  }

  return {
    checks,
    getChecks,
    dialog,
    editedCheck,
    editCheck,
    saveCheck,
  };
});
