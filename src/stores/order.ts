import { computed, ref, watch } from "vue";
import { defineStore } from "pinia";
import orderService from "@/services/order";
import customerService from "@/services/customer";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Order from "@/types/Order";
import type Product from "@/types/Product";
import { useAuthStore } from "./auth";
import { useClose1Store } from "./close1";

export const useOrderStore = defineStore("Order", () => {
  const closeStore = useClose1Store();
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authStore = useAuthStore();
  const dialog = ref(false);
  const istable = ref(true);
  const selected1 = ref([]);
  const selected2 = ref([]);
  const num = ref(0);
  const telephone = ref("");
  const change = ref(0);
  const totalprice = ref(0);
  const discount = ref(0);
  const usepoint = ref(false);
  const orders = ref<Order[]>([]);
  const editedOrder = ref<Order>({});
  const sugarS = ref(0);
  const toppingS = ref(0);
  const sss = ref(0);
  const orderList = ref<
    {
      product: Product;
      amount: number;
      sum: number;
      sugar: number;
      topping: number;
    }[]
  >([]);
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedOrder.value = {};
    }
  });

  function clearOrder() {
    orderList.value = [];
  }

  async function addProduct(item: Product) {
    if (item.catagoryId == 1) {
      closeStore.dialog = true;
      await new Promise<void>((resolve) => {
        const interval = setInterval(() => {
          if (sss.value > 0) {
            clearInterval(interval);
            resolve();
          }
        }, 100);
      });
      sss.value = 0;
    } else {
      sugarS.value = 0;
      toppingS.value = 0;
    }
    for (let i = 0; i < orderList.value.length; i++) {
      if (
        orderList.value[i].product.id === item.id &&
        orderList.value[i].sugar === sugarS.value &&
        orderList.value[i].topping === toppingS.value
      ) {
        orderList.value[i].amount++;
        orderList.value[i].sum =
          orderList.value[i].amount * item.price +
          Number(toppingS.value) * orderList.value[i].amount;
        console.log("IM HERE");
        return;
      }
    }
    orderList.value.push({
      product: item,
      amount: 1,
      sum: Number(toppingS.value) + Number(item.price),
      sugar: sugarS.value,
      topping: toppingS.value,
    });
  }

  function deleteProduct(index: number) {
    console.log("del");
    orderList.value.splice(index, 1);
  }

  const sumAmount = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].amount;
    }
    return sum;
  });

  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].sum;
    }
    return sum;
  });

  async function getOrders() {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getOrders();
      orders.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  /* async function saveOrder() {
      loadingStore.isLoading = true;
      try {
        if (editedOrder.value.id) {
          const res = await orderService.updateOrder(
            editedOrder.value.id,
            editedOrder.value
          );
        } else {
          const res = await orderService.saveOrder(editedOrder.value);
        }
  
        dialog.value = false;
        await getOrders();
      } catch (e) {
        messageStore.showError("ไม่สามารถบันทึก Order ได้");
        console.log(e);
      }
      loadingStore.isLoading = false;
    } */

  async function openOrder() {
    loadingStore.isLoading = false;
    const user: { id: number } = authStore.getUser();
    const orderItems = orderList.value.map(
      (item) =>
        <{ productId: number; amount: number }>{
          productId: item.product.id,
          amount: item.amount,
        }
    );

    const order = {
      userId: user.id,
      orderItems: orderItems,
      received: num.value,
      change: change.value,
      payment: "cash",
      tel: telephone.value,
      discount: discount.value,
    };
    console.log(order);

    try {
      const res = await orderService.saveOrder(order);

      const customerRes = await customerService.getCustomersByTel(
        telephone.value
      );
      console.log(telephone.value);
      if (customerRes.data.length > 0) {
        const customer = customerRes.data[0];
        console.log(discount.value);

        if (discount.value) {
          customer.point -= 10;
          console.log("use point -10");
          messageStore.showError("หักแต้มสะสม 10 Point");
        } else {
          customer.point += 1;
          console.log("no use point +1");
          messageStore.showError("ได้รับแต้มสะสม 1 Point");
        }

        await customerService.updateCustomer(customer.id, customer);
      } else {
        messageStore.showError("ไม่พบข้อมูลลูกค้า");
      }

      dialog.value = false;
      await getOrders();
    } catch (e) {
      console.log(e);
    }

    discount.value = 0;
    telephone.value = "";
    usepoint.value = false;
    loadingStore.isLoading = false;
    clearOrder();
  }

  async function testGetCustomersByTel(tel: string) {
    try {
      const res = await customerService.getCustomersByTel(tel);
      if (res.data.length > 0) {
        const customer = res.data[0];
        console.log(
          "Customer Name: ",
          customer.name,
          "Customer Points: ",
          customer.point
        );
      } else {
        console.log("No customer found with thistelephone number");
      }
    } catch (e) {
      console.log("Error fetching customer by tel", e);
    }
  }

  async function GetPointByTel(tel: string): Promise<number> {
    try {
      const res = await customerService.getCustomersByTel(tel);
      if (res.data.length > 0) {
        const customer = res.data[0];
        console.log("Customer points:", customer.point);
        return customer.point;
      } else {
        console.log("No customer found with this telephone number.");
        return 0;
      }
    } catch (error) {
      console.log("Error fetching customer by tel:", error);
      return 0;
    }
  }

  async function deleteOrder(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.deleteOrder(id);
      await getOrders();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Order ได้");
    }
    loadingStore.isLoading = false;
  }

  async function delOrder() {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.delOrder();
      orders.value = res.data[0];
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Order ได้");
    }
    loadingStore.isLoading = false;
  }

  function editOrder(order: Order) {
    editedOrder.value = JSON.parse(JSON.stringify(order));
    dialog.value = true;
  }
  return {
    orders,
    getOrders,
    dialog,
    istable,
    editedOrder,
    /* saveOrder, */
    editOrder,
    deleteOrder,
    addProduct,
    deleteProduct,
    sumPrice,
    orderList,
    openOrder,
    clearOrder,
    num,
    telephone,
    change,
    discount,
    selected1,
    selected2,
    testGetCustomersByTel,
    usepoint,
    GetPointByTel,
    totalprice,
    delOrder,
    sugarS,
    sss,
    toppingS,
  };
});
