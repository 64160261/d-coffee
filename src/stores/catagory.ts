import { ref, watch } from "vue";
import { defineStore } from "pinia";
import catagoryService from "@/services/catagory";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Catagory from "@/types/Catagory";

export const useCatagoryStore = defineStore("Catagory", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const catagories = ref<Catagory[]>([]);
  const editedCatagory = ref<Catagory>({
    name: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCatagory.value = { name: "" };
    }
  });

  async function getCatagories() {
    loadingStore.isLoading = true;
    try {
      const res = await catagoryService.getCatagories();
      catagories.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Catagory ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveCatagory() {
    loadingStore.isLoading = true;
    try {
      if (editedCatagory.value.id) {
        const res = await catagoryService.updateCatagory(
          editedCatagory.value.id,
          editedCatagory.value
        );
      } else {
        const res = await catagoryService.saveCatagory(editedCatagory.value);
      }

      dialog.value = false;
      await getCatagories();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Catagory ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteCatagory(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await catagoryService.deleteCatagory(id);
      await getCatagories();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Catagory ได้");
    }
    loadingStore.isLoading = false;
  }
  function editCatagory(catagory: Catagory) {
    editedCatagory.value = JSON.parse(JSON.stringify(catagory));
    dialog.value = true;
  }
  return {
    catagories,
    getCatagories,
    dialog,
    editedCatagory,
    saveCatagory,
    editCatagory,
    deleteCatagory,
  };
});
