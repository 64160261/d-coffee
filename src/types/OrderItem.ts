import type Order from "./Order";
import type Product from "./Product";

export default interface OrderItem {
  id?: number;
  name?: string;
  price?: number;
  amount?: number;
  total?: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  order?: Order;
  product?: Product;
}
