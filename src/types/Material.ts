export default interface Material {
  id?: number;
  image?: string;
  name: string;
  minQuantity: number;
  quantity: number;
  unit: number;
  price: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
