export interface Customer {
  id?: number;
  name: string;
  tel: string;
  point?: number;
  start_date?: String;
  createdAt?: String;
  updatedAt?: String;
  deletedAt?: String;
}
