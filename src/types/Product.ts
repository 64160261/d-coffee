import type Catagory from "./Catagory";

export default interface Product {
  id?: number;
  name: string;
  image?: string;
  size: string;
  price: number;
  catagory?: Catagory;
  catagoryId: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
