export default interface Check {
  id?: number;
  date: String;
  time_in: string;
  time_out?: string;
  total_hour?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
