export default interface User {
  id?: number;
  login: string;
  name: string;
  password: string;
  role: string;
  image?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
