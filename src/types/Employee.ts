export interface Employee {
  id?: number;
  name: string;
  tel: string;
  email: string;
  position: string;
  hourly_wage: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
