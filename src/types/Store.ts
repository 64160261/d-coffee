export interface Store {
  id?: number;
  name: string;
  province: string;
  district: string;
  subdistrict: string;
  zip: string;
  tel: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
