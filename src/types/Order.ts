import type { Store } from "pinia";
import type { Customer } from "./Customer";
import type { Employee } from "./Employee";
import type OrderItem from "./OrderItem";

export default interface Order {
  id?: number;
  amount?: number;
  total?: number;
  received?: number;
  discount?: number;
  tel?: string;
  change?: number;
  payment?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  customer?: Customer;
  employee?: Employee;
  store?: Store;
  orderItems?: OrderItem[];
}
