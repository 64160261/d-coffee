import type Product from "@/types/Product";
import http from "./axios";
function getProductsByCatagory(catagory: number) {
  return http.get(`/products/catagory/${catagory}`);
}

function getProducts(params: any) {
  return http.get("/products", { params: params });
}

function getProductsByQuery() {
  return http.get("/reports/product");
}
function getProduct1() {
  return http.get("/reports/product1");
}

function saveProduct(product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("catagoryId", `${product.catagoryId}`);
  formData.append("size", product.size);
  formData.append("price", `${product.price}`);
  formData.append("file", product.files[0]);
  return http.post("/products", formData, {
    headers: { "Content-Type": "multipart/form-data" },
  });
}

function updateProduct(id: number, product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("size", product.size);
  formData.append("price", `${product.price}`);
  if (product.files) {
    formData.append("file", product.files[0]);
  }
  return http.patch(`/products/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function deleteProduct(id: number) {
  return http.delete(`/products/${id}`);
}

export default {
  getProducts,
  getProduct1,
  saveProduct,
  updateProduct,
  deleteProduct,
  getProductsByCatagory,
  getProductsByQuery,
};
