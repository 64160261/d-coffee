import type Catagory from "@/types/Catagory";
import http from "./axios";
function getCatagories() {
  return http.get("/catagories");
}

function saveCatagory(catagory: Catagory) {
  return http.post("/catagories", catagory);
}

function updateCatagory(id: number, catagory: Catagory) {
  return http.patch(`/catagories/${id}`, catagory);
}

function deleteCatagory(id: number) {
  return http.delete(`/catagories/${id}`);
}

export default { getCatagories, saveCatagory, updateCatagory, deleteCatagory };
