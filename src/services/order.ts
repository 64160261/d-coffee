import type Order from "@/types/Order";
import http from "./axios";

function getOrders() {
  return http.get("/orders");
}

function delOrder() {
  return http.delete("/reports/delOrder");
}

function saveOrder(order: {
  userId: number;
  orderItems: { productId: number; amount: number }[];
  received: number;
  discount?: number;
  change: number;
  tel?: string;
}) {
  return http.post("/orders", order);
}

function getcustomerById(customer: number) {
  return http.get(`/orders/catagory/${catagory}`);
}

function updateOrder(id: number, order: Order) {
  return http.patch(`/orders/${id}`, order);
}

function deleteOrder(id: number) {
  return http.delete(`/orders/${id}`);
}

export default {
  getOrders,
  saveOrder,
  updateOrder,
  deleteOrder,
  delOrder,
  getcustomerById,
};
