import type { Store } from "@/types/Store";
import http from "./axios";
function getStores() {
  return http.get("/stores");
}

function saveStores(store: Store) {
  return http.post("/stores", store);
}

function updateStores(id: number, store: Store) {
  return http.patch(`/stores/${id}`, store);
}

function deleteStores(id: number) {
  return http.delete(`/stores/${id}`);
}

export default { getStores, saveStores, updateStores, deleteStores };
