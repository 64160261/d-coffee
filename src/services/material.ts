import type Material from "@/types/Material";
import http from "./axios";

function getMaterials() {
  return http.get("/materials");
}

function getlowMaterial() {
  return http.get("/reports/lowMaterial");
}

function saveMaterial(material: Material & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", material.name);
  formData.append("minQuantity", `${material.minQuantity}`);
  formData.append("quantity", `${material.quantity}`);
  formData.append("unit", `${material.unit}`);
  formData.append("price", `${material.price}`);
  formData.append("file", material.files[0]);
  return http.post("/materials", formData, {
    headers: { "Content-Type": "multipart/form-data" },
  });
}

function updateMaterial(id: number, material: Material & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", material.name);
  formData.append("minQuantity", `${material.minQuantity}`);
  formData.append("quantity", `${material.quantity}`);
  formData.append("unit", `${material.unit}`);
  formData.append("price", `${material.price}`);
  console.log(material.files);
  if (material.files) {
    formData.append("file", material.files[0]);
  }
  return http.patch(`/materials/${id}`, formData, {
    headers: { "Content-Type": "multipart/form-data" },
  });
}

function deleteMaterial(id: number) {
  return http.delete(`/materials/${id}`);
}

export default {
  getMaterials,
  saveMaterial,
  updateMaterial,
  deleteMaterial,
  getlowMaterial,
};
