import type { Customer } from "@/types/Customer";
import http from "./axios";

function getCustomersByTel(tel: string) {
  return http.get(`/customers/tel/${tel}`);
}

function getCustomersByQuery() {
  return http.get("/reports/customer");
}
function getCustomerByPoint() {
  return http.get("/reports/customerbypoint");
}

function getCustomers() {
  return http.get("/customers");
}

function saveCustomer(customer: Customer) {
  return http.post("/customers", customer);
}

function updateCustomer(id: number, customer: Customer) {
  return http.patch(`/customers/${id}`, customer);
}

function deleteCustomer(id: number) {
  return http.delete(`/customers/${id}`);
}

export default {
  getCustomers,
  saveCustomer,
  updateCustomer,
  deleteCustomer,
  getCustomersByTel,
  getCustomersByQuery,
  getCustomerByPoint,
};
