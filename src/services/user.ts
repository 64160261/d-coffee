import type User from "@/types/User";
import http from "./axios";
function getUsers() {
  return http.get("/users");
}

function saveUser(user: User & { files: File[] }) {
  const formData = new FormData();
  formData.append("login", user.login);
  formData.append("name", user.name);
  formData.append("password", user.password);
  formData.append("role", user.role);
  formData.append("file", user.files[0]);
  return http.post("/users", formData, {
    headers: { "Content-Type": "multipart/form-data" },
  });
}

function updateUser(id: number, user: User & { files: File[] }) {
  const formData = new FormData();
  formData.append("login", user.login);
  formData.append("name", user.name);
  formData.append("password", user.password);
  formData.append("role", user.role);
  if (user.files) {
    formData.append("file", user.files[0]);
  }
  return http.patch(`/users/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function deleteUser(id: number) {
  return http.delete(`/users/${id}`);
}

function getUsersByQuery() {
  return http.get("/reports/user");
}

export default { getUsers, saveUser, updateUser, deleteUser, getUsersByQuery };
