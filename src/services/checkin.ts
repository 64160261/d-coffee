import type Check from "@/types/Check";
import http from "./axios";
function getChecks() {
  return http.get("/checkinouts");
}

function saveCheck(check: Check) {
  return http.post("/checkinouts", check);
}

function updateCheck(id: number, check: Check) {
  return http.patch(`/checkinouts/${id}`, check);
}

function deleteCheck(id: number) {
  return http.delete(`/checkinouts/${id}`);
}

export default { getChecks, saveCheck, updateCheck, deleteCheck };
