# Step 1: Build the Vue.js app
FROM node:16-alpine

WORKDIR /app

# Copy package.json and package-lock.json to install dependencies
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application to /app
COPY . .

# Expose the port that the app runs on (usually 3000 for Vite)
EXPOSE 3000

# Start the application in development mode
CMD ["npm", "run", "dev"]
